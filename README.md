This library expose a markdown parser

```mint
Markdown.parse("paragraph") // Result.ok("<p>paragraph</p>")
```

Licensed under _MIT_
