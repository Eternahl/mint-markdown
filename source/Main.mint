module Markdown {
  // Parse markdown string to Html string
  fun parse (raw : String) : Result(Object.Error, String) {
    decode (`marked.parse(#{raw})`) as String
  }
}
