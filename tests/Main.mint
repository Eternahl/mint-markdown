suite "Main" {
  test "Parse" {
    with Test.Context {
      of(
        case (Markdown.parse("paragraph")) {
          Result::Ok(value) => value
          => "[error]"
        }
        |> String.trim())
      |> assertEqual("<p>paragraph</p>")
    }
  }
}
